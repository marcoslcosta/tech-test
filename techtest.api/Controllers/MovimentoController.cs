﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using techtest.application.Interface;
using techtest.domain.Entities;

namespace techtest_api.Controllers
{
    public class MovimentoController : BaseController<Movimento>
    {
        private readonly IMovimentoAppService _appService;

        public MovimentoController(IMovimentoAppService appService)
            : base(appService)
        {

            _appService = appService;
        }
        [HttpGet]
        public HttpResponseMessage ListarUsuariosTipoMovimento()
        {
            try
            {
                var lista = this._appService.ListarUsuariosTipoMovimento();
                var response = Request.CreateResponse(HttpStatusCode.OK, lista);
                return response;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        public HttpResponseMessage ListarAgrupadoUsuariosTipoMovimento()
        {
            try
            {
                var lista = this._appService.ListarAgrupadoUsuariosTipoMovimento();
                var response = Request.CreateResponse(HttpStatusCode.OK, lista);
                return response;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        
    }
}
