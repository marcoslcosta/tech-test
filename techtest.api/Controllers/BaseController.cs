﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using techtest.application.Interface;

namespace techtest_api.Controllers
{
    public class BaseController<TEntity> : ApiController where TEntity : class
    {
        
        private readonly IAppServiceBase<TEntity> _appService;

        public BaseController(IAppServiceBase<TEntity> appService)
        {
            _appService = appService;
        }

        [ActionName("get"), HttpGet]
        public HttpResponseMessage Listar()
        {
            try
            {
                var lista = _appService.GetAll();
                var response = Request.CreateResponse(HttpStatusCode.OK, lista);
                return response;
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

        }

        [HttpGet]
        public TEntity Get(int Id)
        {
            return _appService.GetById(Id);
        }

        [HttpPost]
        public HttpResponseMessage Post(TEntity entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _appService.Add(entity);
                    var response = Request.CreateResponse(HttpStatusCode.Created, entity);
                    return response;

                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ModelState);
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);

            }

        }
        [HttpPut]
        public HttpResponseMessage Put(TEntity entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _appService.Update(entity);
                    var response = Request.CreateResponse(HttpStatusCode.OK, entity);
                    return response;

                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }

        }
        [HttpDelete]
        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var entity = _appService.GetById(Id);
                    _appService.Remove(entity);
                    var entities = _appService.GetAll();
                    var response = Request.CreateResponse(HttpStatusCode.OK, entities);
                    return response;

                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }

        }
    }
}
