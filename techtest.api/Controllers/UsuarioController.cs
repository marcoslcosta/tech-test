﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using techtest.application.Interface;
using techtest.domain.Entities;

namespace techtest_api.Controllers
{

    public class UsuarioController : BaseController<Usuario>
    {

        private readonly IUsuarioAppService _appService;

        public UsuarioController(IUsuarioAppService appService)
            :base(appService)
        {
            _appService = appService;
        }
    }
}
