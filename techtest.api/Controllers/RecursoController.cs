﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using techtest.application.Interface;
using techtest.domain.dto;
using techtest.domain.Entities;

namespace techtest_api.Controllers
{
    [RoutePrefix("recurso")]
    public class RecursoController : BaseController<Recurso>
    {

        private readonly IRecursoAppService _appService;
        private readonly IMovimentoAppService _movimentoAppService;

        public RecursoController(IRecursoAppService appService, IMovimentoAppService movimentoAppService)
            : base(appService)
        {
            this._appService = appService;
            this._movimentoAppService = movimentoAppService;
        }


        [HttpPost]
        public HttpResponseMessage CriarRecursoEMovimento([FromBody]RecursoDto entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _appService.Add(entity.Recurso);

                    var movimento = new Movimento()
                    {
                        RecursoID = entity.Recurso.RecursoID,
                        Quantidade = entity.Quantidade,
                        TipoMovimentoID = 1,
                        UsuarioID = 1

                    };

                    this._movimentoAppService.Add(movimento);


                    var response = Request.CreateResponse(HttpStatusCode.Created, entity);
                    return response;

                }

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ModelState);
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex);

            }

        }

    }
}
