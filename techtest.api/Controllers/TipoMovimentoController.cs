﻿using techtest.application.Interface;
using techtest.domain.Entities;

namespace techtest_api.Controllers
{
    public class TipoMovimentoController : BaseController<TipoMovimento>
    {
        private readonly ITipoMovimentoAppService _appService;

        public TipoMovimentoController(ITipoMovimentoAppService appService)
            :base(appService)
        {
            _appService = appService;
        }
    }
}
