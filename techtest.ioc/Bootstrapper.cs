﻿using SimpleInjector;
using techtest.application;
using techtest.application.Interface;
using techtest.data.Repositories;
using techtest.domain.Interfaces.Repositories;
using techtest.domain.Interfaces.Services;
using techtest.domain.Services;

namespace techtest.ioc
{
    public class Bootstrapper
    {
        public static void RegisterWebApi(Container container)
        {
            //Serviços de Aplicação
            container.Register<IUsuarioAppService, UsuarioAppService>(Lifestyle.Scoped);
            container.Register<IMovimentoAppService, MovimentoAppService>(Lifestyle.Scoped);
            container.Register<ITipoMovimentoAppService, TipoMovimentoAppService>(Lifestyle.Scoped);
            container.Register<IRecursoAppService, RecursoAppService>(Lifestyle.Scoped);

            //Serviços de Domain
            container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);
            container.Register<IMovimentoService, MovimentoService>(Lifestyle.Scoped);
            container.Register<ITipoMovimentoService, TipoMovimentoService>(Lifestyle.Scoped);
            container.Register<IRecursoService, RecursoService>(Lifestyle.Scoped);

            //Serviços de Data
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<IMovimentoRepository, MovimentoRepository>(Lifestyle.Scoped);
            container.Register<ITipoMovimentoRepository, TipoMovimentoRepository>(Lifestyle.Scoped);
            container.Register<IRecursoRepository, RecursoRepository>(Lifestyle.Scoped);
        }
    }
}
