﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace techtest.mvc.Controllers
{
    [RoutePrefix("")]
    public class HomeController : Controller
    {
        // GET: Home
        [Route("")]
        public ActionResult Index()
        {
            return View("NgApp");
        }
       
        [Route("recursos")]
        [Route("usuarios")]
        [Route("home")]
        [Route("movimentacao")]
        [Route("recursos/list")]
        [Route("recursos/edit")]      
        [Route("movimentacao/list")]
        [Route("movimentacao/edit")]
        [Route("movimentacao/logs")]
        [Route("movimentacao/registrarEntrada")]
        [Route("movimentacao/registrarSaida")]        
        public ActionResult AppBookmarkableRoutes()
        {
            return View("NgApp");
        }


    }
}