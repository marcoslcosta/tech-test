﻿import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {
    path: "recursos",
    loadChildren: 'app/recursos/recursos.module#RecursosModule',
  },
  {
    path: "movimentacao",
    loadChildren: 'app/movimentacao/movimentacao.module#MovimentacaoModule',
  },
  {
    path: "usuarios",
    loadChildren: 'app/usuarios/usuarios.module#UsuarioModule',
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: "",
    pathMatch: 'full',
    redirectTo: "/home"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
