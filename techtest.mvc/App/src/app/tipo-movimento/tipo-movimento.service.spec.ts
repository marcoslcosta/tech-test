import { TestBed, inject } from '@angular/core/testing';

import { TipoMovimentoService } from './tipo-movimento.service';

describe('TipoMovimentoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoMovimentoService]
    });
  });

  it('should be created', inject([TipoMovimentoService], (service: TipoMovimentoService) => {
    expect(service).toBeTruthy();
  }));
});
