import { TipoMovimento } from './../data-access-layer/domain/TipoMovimento';
import { ServiceBase } from './../data-access-layer/service-base';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TipoMovimentoService extends ServiceBase<TipoMovimento> {

  constructor(http: Http) {
    super(http, "recurso");
  }


}
