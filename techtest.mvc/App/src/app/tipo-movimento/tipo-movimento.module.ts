import { TipoMovimentoService } from './tipo-movimento.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[
    TipoMovimentoService
  ]
})
export class TipoMovimentoModule { }
