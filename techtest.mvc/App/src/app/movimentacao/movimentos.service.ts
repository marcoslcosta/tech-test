﻿import { Movimento } from './../data-access-layer/domain/Movimento';
import { ServiceBase } from './../data-access-layer/service-base';
import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class MovimentosService extends ServiceBase<Movimento>{

    constructor(http: Http) {
        super(http, "movimento");
    }

    ListarUsuariosTipoMovimento(): Promise<Movimento[]> {
        return new Promise((resolve, reject) => {
            try {
                this.http.get(this.enderecoControler + "/ListarUsuariosTipoMovimento").subscribe((p: any) => {
                    console.log(p);
                    resolve(JSON.parse(p._body));
                }, erro => reject(this.handleError(erro)))
            }
            catch (e) {
                console.log(e);
                reject(e);
            }
        });
    }

    ListarAgrupadoUsuariosTipoMovimento(): Promise<Movimento[]> {
        return new Promise((resolve, reject) => {
            try {
                this.http.get(this.enderecoControler + "/ListarAgrupadoUsuariosTipoMovimento").subscribe((p: any) => {
                    console.log(p);
                    resolve(JSON.parse(p._body));
                }, erro => reject(this.handleError(erro)))
            }
            catch (e) {
                console.log(e);
                reject(e);
            }
        });
    }


    AdicionarItemAoEstoque(movimento: Movimento) {
        return new Promise((resolve, reject) => {
            movimento.tipoMovimentoID = 1;
            movimento.usuarioID = 1;
            this.add(movimento).then(data => resolve(data))
        });
    }

    RemoverItemAoEstoque(movimento: Movimento) {
        return new Promise((resolve, reject) => {
            movimento.tipoMovimentoID = 2;
            movimento.usuarioID = 1;
            this.add(movimento).then(data => resolve(data))
        });
    }

}
