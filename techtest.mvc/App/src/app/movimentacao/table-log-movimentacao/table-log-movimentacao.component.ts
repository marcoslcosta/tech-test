import { Movimento } from './../../data-access-layer/domain/Movimento';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-log-movimentacao',
  templateUrl: './table-log-movimentacao.component.html',
  styleUrls: ['./table-log-movimentacao.component.scss']
})
export class TableLogMovimentacaoComponent implements OnInit {

  @Input()
  public movimento: Movimento[];

  @Output()
  onEdit = new EventEmitter<Movimento>();

  @Output()
  onExcluir = new EventEmitter<Movimento>();

  constructor() { }



  ngOnInit() {
  }

  onEditar(item: Movimento) {
    this.onEdit.emit(item);
  }

  Excluir(item: Movimento) {
    this.onExcluir.emit(item);
  }

}
