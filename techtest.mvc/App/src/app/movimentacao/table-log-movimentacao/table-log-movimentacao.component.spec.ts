import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableMovimentacaoComponent } from './table-movimentacao.component';

describe('TableMovimentacaoComponent', () => {
  let component: TableMovimentacaoComponent;
  let fixture: ComponentFixture<TableMovimentacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableMovimentacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableMovimentacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
