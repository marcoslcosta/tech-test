import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotoesListaComponent } from './botoes-lista.component';

describe('BotoesListaComponent', () => {
  let component: BotoesListaComponent;
  let fixture: ComponentFixture<BotoesListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotoesListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotoesListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
