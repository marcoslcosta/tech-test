import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-botoes-lista',
  templateUrl: './botoes-lista.component.html',
  styleUrls: ['./botoes-lista.component.scss']
})
export class BotoesListaComponent implements OnInit {

  textoBotaoLog = "";
  ngOnInit(): void {

    if (this.router.url == "/movimentacao/logs") {
      this.textoBotaoLog = "Ver quantidade de produtos no estoque";
    }
    else {
      this.textoBotaoLog = "Ver histórico de entrada e saída";
    }

  }

  constructor(private router: Router) { }

  OnEntrada() {
    this.router.navigate(["/movimentacao/registrarEntrada"]);
  }
  OnSaida() {
    this.router.navigate(["/movimentacao/registrarSaida"]);
  }

  OnVerLog() {
    if (this.router.url == "/movimentacao/logs") {
      this.router.navigate(["/movimentacao/list"]);
    }
    else {
      this.router.navigate(["/movimentacao/logs"]);
    }

  }

}
