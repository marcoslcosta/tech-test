import { TableLogMovimentacaoComponent } from './table-log-movimentacao/table-log-movimentacao.component';
import { TipoMovimentoService } from './../tipo-movimento/tipo-movimento.service';
import { RecursosService } from 'app/recursos/recursos.service';
import { MovimentosService } from './movimentos.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MovimentacaoRoutingModule } from './movimentacao-routing.module';
import { MovimentacaoListComponent } from './movimentacao-list/movimentacao-list.component';
import { NgModule } from '@angular/core';
import { EditMovimentacaoComponent } from './edit-movimentacao/edit-movimentacao.component';
import { TableAgrupadaMovimentacaoComponent } from './table-agrupada-movimentacao/table-agrupada-movimentacao.component';
import { MovimentacaoAgrupadaListComponent } from './movimentacao-agrupada-list/movimentacao-agrupada-list.component';
import { BotoesListaComponent } from './botoes-lista/botoes-lista.component';



@NgModule({
    imports: [MovimentacaoRoutingModule, CommonModule, FormsModule],
    exports: [],
    declarations: [MovimentacaoListComponent, TableLogMovimentacaoComponent, EditMovimentacaoComponent, TableAgrupadaMovimentacaoComponent, MovimentacaoAgrupadaListComponent, BotoesListaComponent],
    providers: [MovimentosService, RecursosService, TipoMovimentoService],
})
export class MovimentacaoModule { }
