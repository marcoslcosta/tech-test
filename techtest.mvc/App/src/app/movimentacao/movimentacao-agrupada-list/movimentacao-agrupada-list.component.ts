import { MovimentosService } from './../movimentos.service';
import { Movimento } from './../../data-access-layer/domain/Movimento';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movimentacao-agrupada-list',
  templateUrl: './movimentacao-agrupada-list.component.html',
  styleUrls: ['./movimentacao-agrupada-list.component.scss']
})
export class MovimentacaoAgrupadaListComponent implements OnInit {

 
  errorMessage: any;
  movimentos: Movimento[];

  constructor(private movimentosService: MovimentosService) { }

  ngOnInit() {
    this.listar();
  }

  listar() {
    this.movimentosService.ListarAgrupadoUsuariosTipoMovimento().then(data => this.movimentos = data);
 }


}
