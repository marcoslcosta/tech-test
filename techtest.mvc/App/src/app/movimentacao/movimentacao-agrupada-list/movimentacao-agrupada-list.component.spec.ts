import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentacaoAgrupadaListComponent } from './movimentacao-agrupada-list.component';

describe('MovimentacaoAgrupadaListComponent', () => {
  let component: MovimentacaoAgrupadaListComponent;
  let fixture: ComponentFixture<MovimentacaoAgrupadaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentacaoAgrupadaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentacaoAgrupadaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
