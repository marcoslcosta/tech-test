import { Recurso } from './../../data-access-layer/domain/Recurso';
import { Movimento } from './../../data-access-layer/domain/Movimento';
import { Router } from '@angular/router';
import { MovimentosService } from './../movimentos.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movimentacao-list',
  templateUrl: './movimentacao-list.component.html',
  styleUrls: ['./movimentacao-list.component.scss']
})
export class MovimentacaoListComponent implements OnInit {

  errorMessage: any;
  movimentos: Movimento[];
  movimentoInscricao: Subscription;

  constructor(private movimentosService: MovimentosService, private router: Router) { }

  ngOnInit() {
    this.listar();
  }

  listar() {
    this.movimentosService.ListarUsuariosTipoMovimento().then(data => this.movimentos = data);
 }

  OnEdit(item: Movimento) {
    this.router.navigate(["/movimentacao/edit"],
      {
        queryParams: { "movimentoID": item.movimentoID }
      });
  }

  OnExcluir(item: Movimento) {
    this.movimentosService.delete(item.movimentoID).then(() => this.listar());
  }

  


}
