import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAgrupadaMovimentacaoComponent } from './table-agrupada-movimentacao.component';

describe('TableAgrupadaMovimentacaoComponent', () => {
  let component: TableAgrupadaMovimentacaoComponent;
  let fixture: ComponentFixture<TableAgrupadaMovimentacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAgrupadaMovimentacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAgrupadaMovimentacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
