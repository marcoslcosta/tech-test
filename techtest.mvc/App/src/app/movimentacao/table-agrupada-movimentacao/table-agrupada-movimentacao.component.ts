import { Movimento } from './../../data-access-layer/domain/Movimento';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-agrupada-movimentacao',
  templateUrl: './table-agrupada-movimentacao.component.html',
  styleUrls: ['./table-agrupada-movimentacao.component.scss']
})
export class TableAgrupadaMovimentacaoComponent implements OnInit {

 
  @Input()
  public movimento: Movimento[];

  @Output()
  onEdit = new EventEmitter<Movimento>();

  @Output()
  onExcluir = new EventEmitter<Movimento>();

  constructor() { }



  ngOnInit() {
  }


}
