import { MovimentacaoAgrupadaListComponent } from './movimentacao-agrupada-list/movimentacao-agrupada-list.component';
import { EditMovimentacaoComponent } from './edit-movimentacao/edit-movimentacao.component';
import { MovimentacaoListComponent } from './movimentacao-list/movimentacao-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'edit',
    component: EditMovimentacaoComponent
  },
   {
    path: 'registrarEntrada',
    component: EditMovimentacaoComponent
  },
  {
    path: 'registrarSaida',
    component: EditMovimentacaoComponent
  },
  {
    path: 'edit:id',
    component: EditMovimentacaoComponent
  },
  {
    path: 'logs',
    component: MovimentacaoListComponent
  },
  {
    path: 'list',
    component: MovimentacaoAgrupadaListComponent
  },
  {
    path: "",
    redirectTo: "list"
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovimentacaoRoutingModule { }

export const routedComponents = [MovimentacaoListComponent];