import { TipoMovimento } from './../../data-access-layer/domain/TipoMovimento';
import { TipoMovimentoService } from './../../tipo-movimento/tipo-movimento.service';
import { Subscription } from 'rxjs/Subscription';
import { RecursosService } from 'app/recursos/recursos.service';
import { Recurso } from './../../data-access-layer/domain/Recurso';
import { NgForm } from '@angular/forms';
import { MovimentosService } from './../movimentos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movimento } from './../../data-access-layer/domain/Movimento';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-movimentacao',
  templateUrl: './edit-movimentacao.component.html',
  styleUrls: ['./edit-movimentacao.component.scss']
})
export class EditMovimentacaoComponent implements OnInit {

  movimento = {} as Movimento;
  errorMessage: any;
  titulo: string = "Criar Movimento";

  recursos: Recurso[];
  tipoMovimento: TipoMovimento[];
  movimentos: Movimento[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private route: Router,
    private movimentoService: MovimentosService,
    private tipoMovimentoService: TipoMovimentoService,
    private recursosService: RecursosService) { }

  ngOnInit() {
    this.load();
  }

  onSubmit(f: NgForm) {
    switch (this.route.url) {
      case "/movimentacao/edit": {
        if (Object.keys(this.movimento).length == 0 || this.movimento.movimentoID == 0) {
          this.movimento = f.value as Movimento;
          this.movimentoService.add(this.movimento).then(data => alert("Salvo com sucesso!")).then(() => this.voltar())
        }
        else {
          this.movimento = f.value as Movimento;
          this.movimentoService.put(this.movimento).then(data => alert("Salvo com sucesso!")).then(() => this.voltar())
        }
        break;
      }
      case "/movimentacao/registrarEntrada": {
        this.movimento = f.value as Movimento;
        this.movimentoService.AdicionarItemAoEstoque(this.movimento).then(data => alert("Salvo com sucesso!")).then(() => this.voltar())
        break;
      }
      case "/movimentacao/registrarSaida": {
        this.movimento = f.value as Movimento;
        this.movimentoService.RemoverItemAoEstoque(this.movimento).then(data => alert("Salvo com sucesso!")).then(() => this.voltar())
        break;
      }
    }



  }

  voltar() {
    this.location.back();
  }

  load() {
    this.activatedRoute.queryParams.subscribe((params: any) => {
      if (params && Object.keys(params).length > 0) {
        this.movimentoService.get(params.recursoID).then(data => this.movimento = data);

        this.titulo = "Editar";
      }
      else {
        this.titulo = "Cadastrar"
      }
    });

    this.recursosService.listar().then(data => this.recursos = data)
    this.tipoMovimentoService.listar().then(data => this.tipoMovimento = data);

  }


}
