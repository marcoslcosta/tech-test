import { Recurso } from 'app/data-access-layer/domain/Recurso';
export interface RecursoDto {
    Recurso: Recurso,
    Quantidade: number
}