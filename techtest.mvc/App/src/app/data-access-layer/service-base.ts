import { domainBase } from './domain/domain-base';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

export class ServiceBase<T extends domainBase>{

    private server: string = "http://localhost:50468/api"
    private controller = "/"
    public enderecoControler: string = "";
    private readonly _instance: T;


    public get instance(): T {
        return this._instance;
    }

    constructor(protected http: Http, controller: string) {
        this.controller += controller;
        this.enderecoControler = this.server + this.controller;
    }

    public listar(): Promise<T[]> {
        return new Promise((resolve, reject) => {
            try {
                this.http.get(this.enderecoControler).subscribe((p: any) => {
                    console.log(p);
                    resolve(JSON.parse(p._body));
                }, erro => reject(this.handleError(erro)))
            }
            catch (e) {
                console.log(e);
                reject(e);
            }

        });

    }

    public get(id: number): Promise<T> {

        return new Promise((resolve, reject) => {
            this.http.get(this.enderecoControler + "/" + id).subscribe((p: any) => {
                    console.log(p);
                    resolve(JSON.parse(p._body));
                }, erro => reject(this.handleError(erro)))
        });

    }

    public add(entity: T) {

        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post(this.enderecoControler, entity)
                .subscribe(p => resolve(p), erro => reject(this.handleError(erro)))
        });

    }

    public put(entity: T): Promise<T> {
        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.put(this.enderecoControler, entity, headers)
                .subscribe(p => { console.log(p); resolve(p); }, erro => reject(this.handleError(erro)))
        });
    }


    public delete(id: number): Promise<T> {
        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.delete(this.enderecoControler + "/" + id)
                .subscribe(p => { console.log(p); resolve(p); }, erro => reject(this.handleError(erro)))
        });
    }


    protected extractData(res: Response): T | any {
        let body = res.json();
        return body as T[] || {} as T;
    }


    protected handleError(error: Response | any): Observable<T> {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}