import { domainBase } from './domain-base';
export interface Usuario extends domainBase {
    usuarioID: number;

    nome: string;

    sobrenome: string;

    login: string;

    Senha: string;
}