import { domainBase } from './domain-base';
export interface Recurso extends domainBase {
    recursoID: number;
    descricao: string;
    observacao: string;
}