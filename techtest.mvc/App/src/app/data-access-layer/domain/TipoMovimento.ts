import { domainBase } from './domain-base';
export interface TipoMovimento extends domainBase{
    tipoMovimentoID: number;

    descricao: string;

    sinal: number;
}