import { domainBase } from './domain-base';
import { TipoMovimento } from './TipoMovimento';
import { Recurso } from './Recurso';
import { Usuario } from './Usuario';
export interface Movimento extends domainBase {
    movimentoID: number;

    recursoID: number;

    usuarioID: number;

    tipoMovimentoID: number;

    quantidade: number;

    usuario: Usuario;

    recurso: Recurso;

    tipoMovimento: TipoMovimento;
}