import { EditRecursosComponent } from './edit-recursos/edit-recursos.component';
import { RecursosListComponent } from './recursos-list/recursos-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'edit',
    component: EditRecursosComponent
  },
  {
    path: 'edit:id',
    component: EditRecursosComponent
  },
  {
    path: 'list',
    component: RecursosListComponent
  },
  {
    path: "",
    redirectTo: "list"
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecursosRoutingModule { }
