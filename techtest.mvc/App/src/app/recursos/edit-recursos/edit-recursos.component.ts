import { Recurso } from './../../data-access-layer/domain/Recurso';
import { RecursosService } from 'app/recursos/recursos.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from '@angular/common';


@Component({
  selector: 'app-edit-recursos',
  templateUrl: './edit-recursos.component.html',
  styleUrls: ['./edit-recursos.component.scss']
})
export class EditRecursosComponent implements OnInit {

  recurso = {} as Recurso;
  quantidade: number = 0;
  errorMessage: any;
  titulo: string = "Criar Recurso";
  constructor(private activatedRoute: ActivatedRoute, private location: Location, private recursosService: RecursosService) { }

  ngOnInit() {
    this.load();
  }

  onSubmit(f: NgForm) {
    if (Object.keys(this.recurso).length == 0 || this.recurso.recursoID == 0) {
      this.recurso = {
        descricao: f.value.descricao,
        observacao: f.value.observacao
      } as Recurso;
      this.quantidade = f.value.Quantidade;
      this.recursosService.CriarRecursoEMovimento({
        Recurso: this.recurso,
        Quantidade: this.quantidade
      }).then(() => alert("Salvo com sucesso!"));
    }
    else {
      this.recurso = f.value as Recurso;
      this.recursosService.put(this.recurso)
    }

    this.voltar();
  }

  voltar() {
    this.location.back();
  }

  load() {
    this.activatedRoute.queryParams.subscribe((params: any) => {
      if (params && Object.keys(params).length > 0) {
        this.recursosService.get(params.recursoID).then(data => this.recurso = data);
        this.titulo = "Editar " + this.recurso.descricao || "";
      }
      else {
        this.recurso = {} as Recurso;
        this.titulo = "Cadastrar"
      }
    });
  }

}
