import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRecursosComponent } from './edit-recursos.component';

describe('EditRecursosComponent', () => {
  let component: EditRecursosComponent;
  let fixture: ComponentFixture<EditRecursosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRecursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRecursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
