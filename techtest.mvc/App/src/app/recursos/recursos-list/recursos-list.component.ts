import { Recurso } from './../../data-access-layer/domain/Recurso';
import { Router } from '@angular/router';
import { RecursosService } from './../recursos.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-recursos-list',
  templateUrl: './recursos-list.component.html',
  styleUrls: ['./recursos-list.component.scss']
})
export class RecursosListComponent implements OnInit {
  errorMessage: any;
  recursos: Recurso[];

  constructor(private recursosService: RecursosService, private router: Router) { }

  ngOnInit() {
    this.listar();
  }

  listar() {
    try {

      this.recursosService.listar().then(data => this.recursos = data);
    }
    catch (e) {
      console.error(e);
    }

  }

  OnEdit(item: Recurso) {
    this.router.navigate(["/recursos/edit"],
      {
        queryParams: { "recursoID": item.recursoID }
      });
  }

  OnExcluir(item: Recurso) {
    this.recursosService.delete(item.recursoID);
    this.listar();
  }

  OnAdd() {
    this.router.navigate(["/recursos/edit"]);
  }

  ngOnDestroy() {
  }

}
