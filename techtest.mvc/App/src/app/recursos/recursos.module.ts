import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RecursosService } from './recursos.service';
import { RecursosRoutingModule } from './recursos-routing.module';
import { RecursosListComponent } from './recursos-list/recursos-list.component';
import { NgModule } from '@angular/core';
import { TableRecursosComponent } from './table-recursos/table-recursos.component';
import { EditRecursosComponent } from './edit-recursos/edit-recursos.component';

@NgModule({
    imports: [RecursosRoutingModule, CommonModule, FormsModule],
    exports: [],
    declarations: [RecursosListComponent, TableRecursosComponent, EditRecursosComponent],
    providers: [RecursosService],
})
export class RecursosModule { }
