import { Recurso } from './../data-access-layer/domain/Recurso';
import { RecursoDto } from './../data-access-layer/dto/RecursoDto';
import { ServiceBase } from './../data-access-layer/service-base';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class RecursosService extends ServiceBase<Recurso> {

    constructor(http: Http) {
        super(http, "recurso");
    }

    CriarRecursoEMovimento(recursoDto: RecursoDto):Promise<RecursoDto> {

        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post(this.enderecoControler + "/" + "CriarRecursoEMovimento", recursoDto)
                .subscribe(p => resolve(p), erro => reject(this.handleError(erro)))
        });

    }

}
