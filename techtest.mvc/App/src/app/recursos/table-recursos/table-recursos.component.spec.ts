import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRecursosComponent } from './table-recursos.component';

describe('TableRecursosComponent', () => {
  let component: TableRecursosComponent;
  let fixture: ComponentFixture<TableRecursosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRecursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRecursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
