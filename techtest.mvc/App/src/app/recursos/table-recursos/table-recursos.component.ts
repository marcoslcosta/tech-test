import { Recurso } from './../../data-access-layer/domain/Recurso';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-table-recursos',
  templateUrl: './table-recursos.component.html',
  styleUrls: ['./table-recursos.component.scss']
})
export class TableRecursosComponent implements OnInit {

  constructor() { }

  @Input()
  Recursos: Recurso[];

  @Output()
  onEdit = new EventEmitter<Recurso>();

  @Output()
  onExcluir = new EventEmitter<Recurso>();

  ngOnInit() {
  }

  onEditarRecurso(item: Recurso) {
    this.onEdit.emit(item);
  }

  ExcluirRecurso(item: Recurso) {
    this.onExcluir.emit(item);
  }

}
