import { UsuariosRoutingModule } from './usuario-routing.module';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { NgModule } from '@angular/core';


@NgModule({
    imports: [UsuariosRoutingModule],
    exports: [],
    declarations: [UsuarioListComponent],
    providers: [],
})
export class UsuarioModule { }
