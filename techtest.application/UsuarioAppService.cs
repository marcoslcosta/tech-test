﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using techtest.application.Interface;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Services;

namespace techtest.application
{
    public class UsuarioAppService : AppServiceBase<Usuario>, IUsuarioAppService
    {

        private readonly IUsuarioService _usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService)
            :base(usuarioService)
        {
            this._usuarioService = usuarioService;
        }
    }
}
