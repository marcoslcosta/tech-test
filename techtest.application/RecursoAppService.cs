﻿using techtest.application.Interface;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Services;

namespace techtest.application
{
    public class RecursoAppService : AppServiceBase<Recurso>, IRecursoAppService
    {
        private readonly IRecursoService _recursoService;

        public RecursoAppService(IRecursoService recursoService)
            :base(recursoService)
        {
            this._recursoService = recursoService;
        }
    }
}
