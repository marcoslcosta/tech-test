﻿using techtest.application.Interface;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Services;

namespace techtest.application
{
    public class TipoMovimentoAppService : AppServiceBase<TipoMovimento>, ITipoMovimentoAppService
    {
        private readonly ITipoMovimentoService _tipoMovimentoService;

        public TipoMovimentoAppService(ITipoMovimentoService tipoMovimentoService)
            : base(tipoMovimentoService)
        {
            this._tipoMovimentoService = tipoMovimentoService;
        }
    }
}
