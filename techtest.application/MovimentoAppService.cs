﻿using System;
using System.Collections.Generic;
using techtest.application.Interface;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Services;

namespace techtest.application
{
    public class MovimentoAppService : AppServiceBase<Movimento>, IMovimentoAppService
    {
        private readonly IMovimentoService _movimentoService;

        public MovimentoAppService(IMovimentoService movimentoService)
            :base(movimentoService)
        {
            this._movimentoService = movimentoService;
        }

        public IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento()
        {
            return _movimentoService.ListarAgrupadoUsuariosTipoMovimento();
        }

        public IEnumerable<Movimento> ListarUsuariosTipoMovimento()
        {
            return this._movimentoService.ListarUsuariosTipoMovimento();
        }
    }
}
