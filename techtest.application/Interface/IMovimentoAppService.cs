﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using techtest.domain.Entities;

namespace techtest.application.Interface
{
    public interface IMovimentoAppService : IAppServiceBase<Movimento>
    {
        IEnumerable<Movimento> ListarUsuariosTipoMovimento();

        IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento();
    }
}
