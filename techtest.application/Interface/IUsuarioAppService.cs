﻿using techtest.domain.Entities;

namespace techtest.application.Interface
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario>
    {
    }
}
