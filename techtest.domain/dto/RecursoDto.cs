﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using techtest.domain.Entities;

namespace techtest.domain.dto
{
    public class RecursoDto
    {
        public Recurso Recurso { get; set; }
        public decimal Quantidade { get; set; }
    }
}
