﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace techtest.domain.Entities
{
    public class TipoMovimento
    {
        public int TipoMovimentoID { get; set; }

        public string Descricao { get; set; }

        public int sinal { get; set; }
    }
}
