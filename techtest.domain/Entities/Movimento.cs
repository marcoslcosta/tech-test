﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace techtest.domain.Entities
{
    public class Movimento
    {
        public int MovimentoID { get; set; }

        public int RecursoID { get; set; }

        public int UsuarioID { get; set; }

        public int TipoMovimentoID { get; set; }

        public decimal Quantidade { get; set; }


        public Usuario Usuario { get; set; }

        public Recurso Recurso { get; set; }

        public TipoMovimento TipoMovimento { get; set; }
    }
}
