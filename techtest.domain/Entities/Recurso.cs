﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace techtest.domain.Entities
{
    public class Recurso
    {
        public int RecursoID { get; set; }

        public string Descricao { get; set; }

        public string Observacao { get; set; }
    }
}
