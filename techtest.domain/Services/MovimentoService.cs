﻿using System;
using System.Collections.Generic;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;
using techtest.domain.Interfaces.Services;

namespace techtest.domain.Services
{
    public class MovimentoService : ServiceBase<Movimento>, IMovimentoService  {

        private readonly IMovimentoRepository _movimentoRepository;

        public MovimentoService(IMovimentoRepository clienteRepository) 
            : base(clienteRepository)
        {
            _movimentoRepository = clienteRepository;
        }

        public IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento()
        {
            return _movimentoRepository.ListarAgrupadoUsuariosTipoMovimento();
        }

        public IEnumerable<Movimento> ListarUsuariosTipoMovimento()
        {
            return _movimentoRepository.ListarUsuariosTipoMovimento();
        }
    }
}
