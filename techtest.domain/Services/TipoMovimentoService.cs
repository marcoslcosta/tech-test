﻿
using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;
using techtest.domain.Interfaces.Services;

namespace techtest.domain.Services
{
    public class TipoMovimentoService : ServiceBase<TipoMovimento>, ITipoMovimentoService
    {
        private readonly ITipoMovimentoRepository _tipoMovimentoRepository;

        public TipoMovimentoService(ITipoMovimentoRepository tipoMovimentoRepository) 
            : base(tipoMovimentoRepository)
        {
            _tipoMovimentoRepository = tipoMovimentoRepository;
        }
    }
}
