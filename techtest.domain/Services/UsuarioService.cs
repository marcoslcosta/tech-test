﻿
using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;
using techtest.domain.Interfaces.Services;

namespace techtest.domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository) 
            : base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }
    }
}
