﻿using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;
using techtest.domain.Interfaces.Services;

namespace techtest.domain.Services
{
    public class RecursoService: ServiceBase<Recurso>, IRecursoService
    {
        private readonly IRecursoRepository _recursoRepository;

        public RecursoService(IRecursoRepository recursoRepository) 
            : base(recursoRepository)
        {
            _recursoRepository = recursoRepository;
        }
        
    }
}
