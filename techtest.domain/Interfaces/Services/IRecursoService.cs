﻿using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Services
{
    public interface IRecursoService : IServiceBase<Recurso>
    {
    }
}
