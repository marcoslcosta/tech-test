﻿using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Services
{
    public interface IUsuarioService : IServiceBase<Usuario>
    {
    }
}
