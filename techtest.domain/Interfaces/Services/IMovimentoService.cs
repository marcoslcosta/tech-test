﻿using System.Collections.Generic;
using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Services
{
    public interface IMovimentoService : IServiceBase<Movimento>
    {
        IEnumerable<Movimento> ListarUsuariosTipoMovimento();

        IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento();

    }
}
