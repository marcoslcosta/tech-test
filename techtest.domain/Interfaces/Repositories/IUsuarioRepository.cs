﻿using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
    }
}
