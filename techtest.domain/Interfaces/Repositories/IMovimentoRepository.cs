﻿using System.Collections.Generic;
using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Repositories
{
    public interface IMovimentoRepository : IRepositoryBase<Movimento>
    {
        IEnumerable<Movimento> ListarUsuariosTipoMovimento();

        IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento();
    }
}
