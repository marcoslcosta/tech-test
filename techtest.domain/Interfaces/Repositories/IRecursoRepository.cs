﻿using techtest.domain.Entities;

namespace techtest.domain.Interfaces.Repositories
{
    public interface IRecursoRepository : IRepositoryBase<Recurso>
    {
    }
}
