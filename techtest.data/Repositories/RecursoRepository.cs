﻿using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;

namespace techtest.data.Repositories
{
    public class RecursoRepository : RepositoryBase<Recurso>, IRecursoRepository
    {
    }
}
