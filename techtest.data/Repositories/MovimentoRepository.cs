﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using techtest.data.Contexto;
using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;

namespace techtest.data.Repositories
{
    public class MovimentoRepository : RepositoryBase<Movimento>, IMovimentoRepository
    {
        public IEnumerable<Movimento> ListarUsuariosTipoMovimento()
        {
            using (var Contexto = new ProjetoModeloContext())
            {
                var movimento = Contexto.Movimentos
                    .Include(x => x.TipoMovimento)
                    .Include(x => x.Usuario)
                    .Include(x => x.Recurso);

                var lista = movimento.ToList();
                return lista;
            }
        }

        public IEnumerable<Movimento> ListarAgrupadoUsuariosTipoMovimento()
        {
            using (var Contexto = new ProjetoModeloContext())
            {

                var movimento = Contexto.Movimentos
                                 .Include(x => x.TipoMovimento)
                                 .Include(x => x.Usuario)
                                 .Include(x => x.Recurso);

                var movimentoGroupBy = movimento
                                 .GroupBy(p => new { p.Recurso, p.Usuario })
                                 .Select(g => new
                                 {
                                     item = g.Key,
                                     Quantidade = g.Sum(ri => ri.Quantidade * ri.TipoMovimento.sinal)
                                 });

                var movimentosSelecionados = movimentoGroupBy.ToList().Select(p =>
                new Movimento
                {
                    Usuario = p.item.Usuario,
                    Recurso = p.item.Recurso,
                    RecursoID = p.item.Recurso.RecursoID,
                    UsuarioID = p.item.Usuario.UsuarioID,
                    Quantidade = p.Quantidade
                });

                var lista = movimentosSelecionados;
                return lista;
            }
        }
    }
}
