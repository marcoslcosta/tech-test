﻿using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;

namespace techtest.data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
    }
}
