﻿using techtest.domain.Entities;
using techtest.domain.Interfaces.Repositories;

namespace techtest.data.Repositories
{
   public class TipoMovimentoRepository : RepositoryBase<TipoMovimento>, ITipoMovimentoRepository
    {
    }
}
