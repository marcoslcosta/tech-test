namespace techtest.data.Migrations
{
    using System.Data.Entity.Migrations;
    using techtest.domain.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<techtest.data.Contexto.ProjetoModeloContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(techtest.data.Contexto.ProjetoModeloContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.TipoMovimentos.AddOrUpdate(p => p.TipoMovimentoID, 
                new TipoMovimento { Descricao = "Adiciona Estoque", sinal = 1 },
                new TipoMovimento { Descricao = "Remove Estoque", sinal = -1 });

            context.Usuarios.AddOrUpdate(p => p.UsuarioID,
              new Usuario { Nome = "Administrador", Sobrenome = "do sistema", Login = "admin", Senha = "admin" });
        }
    }
}
